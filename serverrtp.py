#!/usr/bin/python3
# -*- coding: utf-8 -*-
import socketserver
import sys
import threading
import socket
import simplertp
import time

usage = 'Usage: python3 serverrtp.py <IPServerSIP>:<portServerSIP> <service>'
IP = "127.0.0.1"
PORT = 6002


def historicos():
    date = time.strftime('%Y%m%d%H%M%S')
    return date


def conseguir_argv():
    try:
        IpServerSIP = sys.argv[1].split(':')[0]
        portServerSIP = int(sys.argv[1].split(':')[1])
        service = sys.argv[2]  # usuario

    except:
        sys.exit(usage)

    return IpServerSIP, portServerSIP, service

#rcv_rtp.py

class RTPHandler(socketserver.BaseRequestHandler):
    """Clase para manejar los paquetes RTP que nos lleguen.

    Atención: Heredamos de BaseRequestHandler porque UDPRequestHandler
    siempre termina enviando una respuesta al cliente, algo que aquí no
    queremos hacer. Al usar BaseRequestHandler no podemos usar self.rfile
    (porque esta clase no lo tiene), así que leemos el mensaje directamente
    de self.request (que es una lista, cuyo primer elemento es el mensaje
    recibido)"""

    def handle(self):
        msg = self.request[0]
        # sólo nos interesa lo que hay a partir del byte 54 del paquete UDP
        # que es donde está la payload del paquete RTP qeu va dentro de él
        payload = msg[12:]
        # Escribimos esta payload en el fichero de salida
        self.output.write(payload)
        print("Received") #bucle de RECEIVED

    @classmethod
    def open_output(cls, filename):
        """Abrir el fichero donde se va a escribir lo recibido.

        Lo abrimos en modo escritura (w) y binario (b)
        Es un método de la clase para que podamos invocarlo sobre la clase
        antes de empezar a recibir paquetes."""

        cls.output = open(filename, 'wb')

    @classmethod
    def close_output(cls):
        """Cerrar el fichero donde se va a escribir lo recibido.

        Es un método de la clase para que podamos invocarlo sobre la clase
        después de terminar de recibir paquetes."""
        cls.output.close()


def main():  # Lanzar servidor RTP
    IpServerSIP, portServerSIP, service = conseguir_argv()

    try:
        my_socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        print(f'{historicos()} Starting...')

        message = f'REGISTER sip:{service}@songs.net SIP/2.0\r\n\r\n'
        my_socket.sendto(message.encode('utf-8'), (IpServerSIP, portServerSIP))
        print(f'{historicos()} SIP to {IpServerSIP}:{portServerSIP}:\r\n{message}')
        data, address = my_socket.recvfrom(1024)
        print(f'{historicos()} SIP from {address[0]}:{address[1]}: {data.decode("utf-8")}')

        received = data.decode('utf-8')
        petition = received.split()[0]

        if received == 'SIP/2.0 200 OK\r\n\r\n':
            data2, address = my_socket.recvfrom(1024)
            received2 = data2.decode('utf-8')
            petition = received2.split()[0]
            print(f'{historicos()} SIP from {address[0]}:{address[1]}:\r\n{received2}')  # Histórico
            if petition == 'INVITE':  # cuando reciba los invite se prapara para recibir PAQUETES RTP. RESPONDERA CON OK
                global client_ip
                global client_port
                client_ip = received2.split('o=')[1].split(' ')[1].split('\r\n')[0]
                client_port = int(received2.split('m=audio ')[1].split(' ')[0])  # 34543

                response = f'SIP/2.0 200 OK\r\nFrom: <servidor1@songs.net>'  # servidor1
                content_type = 'Content-Type: application/sdp'
                session_name = received2.split("s=")[1].split("\r\n")[0]  # cliente1
                originador = received2.split("o=")[1].split("\r\n")[0]
                sdp_elements = f'v=0\r\no={originador}\r\ns={session_name}\r\nt=0\r\nm=audio {PORT} RTP'
                message = f'{response}\r\n{content_type}\r\n\r\n{sdp_elements}'
                my_socket.sendto(message.encode('utf-8'), (address[0], address[1]))

                print(f'{historicos()} SIP to {address[0]}:{address[1]}:\r\n{response}')  # Histórico

                RTPHandler.open_output('recibido.mp3')
                with socketserver.UDPServer((IP, PORT), RTPHandler) as serv:
                    print("Listening...")
                    # El bucle de recepción (serv_forever) va en un hilo aparte,
                    # para que se continue ejecutando este programa principal,
                    # y podamos interrumpir ese bucle más adelante
                    threading.Thread(target=serv.serve_forever).start()
                    # Paramos un rato. Igualmente podríamos esperar a recibir BYE,
                    # por ejemplo
                    time.sleep(10)
                    print("Time passed, shutting down receiver loop.")
                    # Paramos el bucle de recepción, con lo que terminará el thread,
                    # y dejamos de recibir paquetes
                    serv.shutdown()
                # Cerramos el fichero donde estamos escribiendo los datos recibidos

                data3, address = my_socket.recvfrom(1024)
                received3 = data3.decode('utf-8')
                print(f'{historicos()} SIP from {address[0]}:{address[1]}: {data.decode("utf-8")}')
                petition3 = received3.split()[0]
                if petition3 == 'BYE':
                    serv.shutdown()
                    RTPHandler.close_output()
                    # enviar ok al cliente
                    ok = 'SIP/2.0 200 OK'
                    From = f'From: <sip:servidor1@songs.net\r\n\r\n>'
                    message = f'{ok}\r\n{From}'
                    my_socket.sendto(message.encode('utf-8'), (address[0], address[1]))
                    print(f'{historicos()} SIP to {address[0]}:{address[1]}:\r\n{message}')

                    return main()  # recursion

    except OSError as e:
        sys.exit(f"Error while trying to listen: {e.args[1]}.")


if __name__ == '__main__':
    main()
