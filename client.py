#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""
Simple client for a SIP registrar
"""
import socketserver
import socket
import sys
import time
import threading
import simplertp

usage = 'Usage: python3 client.py <IPServerSIP>:<portServerSIP> <addrClient> <addrServerRTP>  <file>'
IP = '127.0.0.1'
PORT = 45654

def historicos():  #tiempo
    date = time.strftime('%Y%m%d%H%M%S')
    return date

def get_arguments():
    try:
        IpServerSIP = sys.argv[1].split(':')[0]
        portServerSIP = int(sys.argv[1].split(':')[1])
        addrClient = sys.argv[2]
        addrServerRTP = sys.argv[3]
        file = sys.argv[4]
    except:
        sys.exit(usage)
    return IpServerSIP, portServerSIP, addrClient, addrServerRTP, file


#MAIN
def main():
    IpServerSIP, portServerSIP, addrClient, addrServerRTP, file = get_arguments()

    try:
        with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as my_socket:
            print(f'{historicos()} Starting...')
            register = f'REGISTER {addrClient} SIP/2.0\r\n\r\n'
            my_socket.sendto(register.encode('utf-8'), (IpServerSIP, portServerSIP))
            print(f'{historicos()} SIP to {IpServerSIP}:{portServerSIP}: {register}')

            data, address = my_socket.recvfrom(1024)
            mssg = data.decode('utf-8')
            print(f'{historicos()} SIP from {address[0]}:{address[1]}: {mssg}')

            if data.decode("utf-8") == 'SIP/2.0 200 OK\r\n\r\n':
                protocol = f'INVITE {addrServerRTP} SIP/2.0'
                From = f'From: <{addrClient}>'
                content_type = 'Content-Type: application/sdp'

                session_name = addrClient.split("@")[0].split(":")[1]
                sdp_elements = f'v=0\r\no={addrClient} 127.0.0.1\r\ns={session_name}\r\nt=0\r\nm=audio {PORT} RTP'
                message = f'{protocol}\r\n{From}\r\n{content_type}\r\n\r\n{sdp_elements}'
                my_socket.sendto(message.encode('utf-8'), (IpServerSIP, portServerSIP))
                print(f'{historicos()} SIP to {address[0]}:{address[1]}: {message}')

                data2, address = my_socket.recvfrom(1024)
                mssg2 = data2.decode('utf-8')
                print(f'{historicos()} SIP from {address[0]}:{address[1]}:\r\n{mssg2}')

                if data2.decode('utf-8').split('\r\n')[0] == 'SIP/2.0 302 Moved Temporarily':


                    nueva_dic = data2.decode('utf-8').split('\r\n')[1].split()[1]
                    nueva_dic_IP = nueva_dic.split('@')[1].split(':')[0]
                    nueva_dic_PORT = int(nueva_dic.split('@')[1].split(':')[1])

                    ack = f'ACK {addrServerRTP} SIP/2.0'
                    From = f'From: <{addrClient}>\r\n\r\n'
                    message1 = f'{ack}\r\n{From}'
                    my_socket.sendto(message1.encode('utf-8'), (IpServerSIP, portServerSIP))
                    print(f'{historicos()} SIP to {IpServerSIP}:{portServerSIP}:\r\n{message1}')

                    protocol = f'INVITE {nueva_dic} SIP/2.0'
                    From = f'From: <{addrClient}>'
                    content_type = 'Content-Type: application/sdp'

                    session_name = addrClient.split("@")[0].split(":")[1]
                    sdp_elements = f'v=0\r\no={addrClient} 127.0.0.1\r\ns={session_name}\r\nt=0\r\nm=audio {PORT} RTP'
                    message = f'{protocol}\r\n{From}\r\n{content_type}\r\n\r\n{sdp_elements}'
                    my_socket.sendto(message.encode('utf-8'), (nueva_dic_IP, nueva_dic_PORT))
                    print(f'{historicos()} SIP to {nueva_dic_IP}:{nueva_dic_PORT}:\r\n{message}')

                    data, address = my_socket.recvfrom(1024)
                    mensaje = data.decode('utf-8')
                    confirmacion = mensaje.split('\r\n')[0]
                    print(f'{historicos()} SIP from {address[0]}:{address[1]}:\r\n{data.decode("utf-8")}')

                    if confirmacion == 'SIP/2.0 200 OK':
                        ack = f'ACK {nueva_dic} SIP/2.0'
                        From = f'From: <{addrClient}>\r\n\r\n'
                        message = f'{ack}\r\n{From}'
                        my_socket.sendto(message.encode('utf-8'), (IpServerSIP, portServerSIP))
                        print(f'{historicos()} SIP to {IpServerSIP}:{portServerSIP}:\r\n{message}')

                        port_client = mensaje.split("m=audio ")[1].split(' ')[0]
                        audio_file = sys.argv[4]
                        print(f"RTP to {nueva_dic_IP}:{nueva_dic_PORT}")
                        sender = simplertp.RTPSender(ip=nueva_dic_IP, port=int(port_client), file=audio_file, printout=True)
                        sender.send_threaded()
                        time.sleep(30)
                        print("Finalizing the sending thread.")
                        sender.finish()

                        print(f'---CLOSING RTP PACKET SENDING---')
                        server1 = nueva_dic.split('@')[0]
                        message = f'BYE {server1}@{nueva_dic_IP}:{nueva_dic_PORT} SIP/2.0\r\n\r\n'
                        my_socket.sendto(message.encode('utf-8'), (nueva_dic_IP, nueva_dic_PORT))
                        print(f'{historicos()} SIP to {nueva_dic_IP}:{nueva_dic_PORT}:\r\n{message}')

                        data = my_socket.recv(1024)
                        mensajertp = data.decode('utf-8')
                        ok_rtp = mensajertp.split('\r\n')[0]
                        if ok_rtp == 'SIP/2.0 200 OK':
                            print("------Server done-------")
                            my_socket.close()

    except ConnectionRefusedError:
        print("Error connecting to server")


if __name__ == "__main__":
    main()
